<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181025122907 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD phone VARCHAR(50) DEFAULT NULL, ADD current_location VARCHAR(50) DEFAULT NULL, ADD adress VARCHAR(255) DEFAULT NULL, ADD gender VARCHAR(10) DEFAULT NULL, ADD country VARCHAR(50) DEFAULT NULL, ADD nationality VARCHAR(50) DEFAULT NULL, ADD passport LONGTEXT DEFAULT NULL, ADD cv LONGTEXT DEFAULT NULL, ADD picture LONGTEXT DEFAULT NULL, ADD birthdate DATE DEFAULT NULL, ADD job_sector VARCHAR(155) DEFAULT NULL, ADD experience VARCHAR(155) DEFAULT NULL, ADD short_description LONGTEXT DEFAULT NULL, ADD notes LONGTEXT DEFAULT NULL, ADD date_created_at DATETIME NOT NULL, ADD date_updated_at DATETIME DEFAULT NULL, ADD date_deleted_at DATETIME DEFAULT NULL, CHANGE roles roles VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP phone, DROP current_location, DROP adress, DROP gender, DROP country, DROP nationality, DROP passport, DROP cv, DROP picture, DROP birthdate, DROP job_sector, DROP experience, DROP short_description, DROP notes, DROP date_created_at, DROP date_updated_at, DROP date_deleted_at, CHANGE roles roles TEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
